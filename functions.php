<?php

//Enqueue scripts and styles
function citadel_adding_scripts() {
	wp_enqueue_style('style', get_template_directory_uri() . '/style.css');
	wp_enqueue_script( 'customjs', get_template_directory_uri() . '/assets/js/scripts.js', array('jquery'), '', true );

	$translation_array = array( 'templateUrl' => get_template_directory_uri() );
	wp_localize_script( 'customjs', 'template_directory', $translation_array );
}
add_action( 'wp_enqueue_scripts', 'citadel_adding_scripts' );

// Add post thumbnail feature
//add_theme_support( 'post-thumbnails' );

// Customizer settings
include('inc/customizer.php');

// Custom WP admin settings
include('inc/admin.php');

// Default new WPMU settings
include('inc/musettings.php');

// Add New Menus & Widget Areas
register_nav_menus( array(
	'primary' 		=> 'Citadel Main Menu',
	'legalfooter' 	=> 'Citadel Legal Footer Menu',
) );

// Remove Dashboard Widgets
function remove_dashboard_widgets() {
	remove_meta_box( 'dashboard_activity', 'dashboard', 'normal' );   // Recent Activity
	remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' ); // Recent Comments
	remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );  // Incoming Links
	remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );   // Plugins
	remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );  // Quick Press
	remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );  // Recent Drafts
	remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );   // WordPress blog
	remove_meta_box( 'dashboard_secondary', 'dashboard', 'side' );   // Other WordPress News
	remove_meta_box('dashboard_right_now', 'dashboard', 'normal'); //Removes the 'At a Glance' widget
}
add_action( 'wp_dashboard_setup', 'remove_dashboard_widgets' );

remove_action( 'welcome_panel', 'wp_welcome_panel' );
remove_action( 'try_gutenberg_panel', 'wp_try_gutenberg_panel' );

add_action('wp_network_dashboard_setup', 'hhs_remove_network_dashboard_widgets' );
function hhs_remove_network_dashboard_widgets() {
	remove_meta_box ( 'network_dashboard_right_now', 'dashboard-network', 'normal' ); // Right Now
	remove_meta_box ( 'dashboard_plugins', 'dashboard-network', 'normal' ); // Plugins
	remove_meta_box ( 'dashboard_primary', 'dashboard-network', 'side' ); // WordPress Blog
	remove_meta_box ( 'dashboard_secondary', 'dashboard-network', 'side' ); // Other WordPress News
}

function wp_get_cat_postcount($id) {
	$cat = get_category($id);
	$count = (int) $cat->count;
	$taxonomy = 'category';
	$args = array(
		'child_of' => $id,
	);
	$tax_terms = get_terms($taxonomy,$args);
	foreach ($tax_terms as $tax_term) {
		$count +=$tax_term->count;
	}

	return $count;
}

// Add Ticket post type
function create_post_type() {
  register_post_type( 'citadel_ticket',
	array(
		'exclude_from_search' => true,
		'has_archive' => true,
		'hierarchal' => false,
		'labels' => array(
			'name' => __( 'Tickets' ),
			'singular_name' => __( 'Ticket' ),
			'menu_name' => __( 'Tickets' ),
			'parent_item_colon' => __( 'Ticket:' ),
			'all_items' => __( 'All Tickets' ),
			'view_item' => __( 'View Ticket' ),
			'add_new_item' => __( 'Add New Ticket' ),
			'add_new' => __( 'New Ticket' ),
			'edit_item' => __( 'Edit Ticket' ),
			'update_item' => __( 'Update Ticket' ),
			'search_items' => __( 'Search Tickets' ),
			'not_found' => __( 'No tickets found' ),
			'not_found_in_trash' => __( 'No tickets found in Trash' ),
		),
		'menu_icon' => 'dashicons-tickets-alt',
		'public' => true,
		'publicly_queryable' => true,
		'query_var' => true,
		'rewrite' => array('slug' => 'tickets'),
		'show_in_rest' => true,
		'supports' => array( 'title', 'editor', ),
	)
  );
}
add_action( 'init', 'create_post_type' );

function create_ticket_taxonomies() {
	register_taxonomy(
		'ticket_categories',
		'citadel_ticket',
		array(
			'label' => __( 'Status' ),
			'rewrite' => array( 'slug' => 'tickets/status' ),
			'hierarchical' => true,
			'show_in_rest' => true,
		)
	);

	register_taxonomy(
		'ticket_types',
		'citadel_ticket',
		array(
			'label' => __( 'Type' ),
			'rewrite' => array( 'slug' => 'tickets/type' ),
			'hierarchical' => true,
			'show_in_rest' => true,
		)
	);
}
add_action( 'init', 'create_ticket_taxonomies' );

function _post_type_rewrite() {
global $wp_rewrite;

// Set the query arguments used by WordPress
$queryarg = 'post_type=citadel_ticket&p=';

// Concatenate %cpt_id% to $queryarg (eg.. &p=123)
$wp_rewrite->add_rewrite_tag( '%cpt_id%', '([^/]+)', $queryarg );

// Add the permalink structure
$wp_rewrite->add_permastruct( 'citadel_ticket', '/ticket/%cpt_id%/', false );
}
  add_action( 'init', '_post_type_rewrite' );

/**
  * Replace permalink segment with post ID
  *
  */
function _post_type_permalink( $post_link, $id = 0, $leavename ) {
global $wp_rewrite;
$post = get_post( $id );
if ( is_wp_error( $post ) )
	return $post;

	// Get post permalink (should be something like /some-type/%cpt_id%/
	$newlink = $wp_rewrite->get_extra_permastruct( 'citadel_ticket' );

	// Replace %cpt_id% in permalink structure with actual post ID
	$newlink = str_replace( '%cpt_id%', $post->ID, $newlink );
	$newlink = home_url( user_trailingslashit( $newlink ) );
return $newlink;
  }
 add_filter('post_type_link', '_post_type_permalink', 1, 3);


 // Add user metabox
 function citadel_submitter_meta_box() {

	add_meta_box(
		'citadel_ticket_submitter',
		'Submitter Email',
		'citadel_ticket_submitter',
		'citadel_ticket',
		'side',
		'default',
		array(
			'__block_editor_compatible_meta_box' => true,
		)
	);

	add_meta_box(
		'citadel_ticket_submitter_name',
		'Submitter Name',
		'citadel_ticket_submitter_name',
		'citadel_ticket',
		'side',
		'default',
		array(
			'__block_editor_compatible_meta_box' => true,
		)
	);
}
add_action( 'add_meta_boxes', 'citadel_submitter_meta_box' );

function citadel_ticket_submitter($post) {
	$value = get_post_meta($post->ID, 'citadel_submitter_key', true);
	?>
	<label for="ticket_submitter">Ticket Submitter Email</label>
	<input type="text" name="ticket_submitter" value="<?php echo $value; ?>" class="widefat"/>
	<?php
}

function citadel_ticket_submitter_name($post) {
	$value = get_post_meta($post->ID, 'citadel_submitter_name_key', true);
	?>
	<label for="ticket_submitter_name">Ticket Submitter Name</label>
	<input type="text" name="ticket_submitter_name" value="<?php echo $value; ?>" class="widefat"/>
	<?php
}

function citadel_save_postdata($post_id)
{
	if (array_key_exists('ticket_submitter', $_POST)) {
		update_post_meta(
			$post_id,
			'citadel_submitter_key',
			$_POST['ticket_submitter']
		);
	}

	if (array_key_exists('ticket_submitter_name', $_POST)) {
		update_post_meta(
			$post_id,
			'citadel_submitter_name_key',
			$_POST['ticket_submitter_name']
		);
	}
}
add_action('save_post', 'citadel_save_postdata');


// Submit Ticket
add_shortcode( 'citadel_ticket_submit', 'citadel_ticket_submit' );
function citadel_ticket_submit() {
	citadel_save_ticket_if_submitted();
	?>
<form method="post" id="submitTicket" name="submitTicket">
	<p class="form-info">* All fields are required</p>
	<p>
		<label for="submitter">Submitter Citadel Username *</label>
		<small>The first part of your default Citadel email.</small>
		<input type="text" name="submitter" id="submitter" required />
	</p>

	<p>
		<label for="submitter_name">Submitter Name *</label>
		<small>Your first and last name.</small>
		<input type="text" name="submitter_name" id="submitter_name" required />
	</p>

	<p>
		<label for="title">Ticket Subject *</label>
		<small>A short title for the request/issue.</small>
		<input type="text" name="title" id="title" required/>
	</p>

	<p>
		<label for="type">Ticket Type *</label>
		<small>Type of request/issue.</small>
		<select name="type" id="type" required>
			<option value="select" disabled>Select</option>
			<?php
				$args = array(
						   'taxonomy' 	=> 'ticket_types',
						   'orderby' 	=> 'name',
						   'order'   	=> 'ASC',
						   'hide_empty'	=> 0,
					   );

				$cats = get_categories($args);

				foreach($cats as $cat) {
					$category_link = get_category_link( $cat->cat_ID );
			?>

			<option value="<?php echo $cat->slug; ?>"><?php echo $cat->name; ?></option>

			<?php } ?>
		</select>
	</p>

	<p>
		<label for="content">Description *</label>
		<small>A description of the request/issue.</small>
		<textarea type="text" name="content" id="content" required></textarea>
	</p>

	<?php wp_nonce_field('submit_ticket'); ?>

	<input type="text" id="website" name="website"/>

	<input type="hidden" name="status" id="status" value="Open" />

	<button type="submit" id="submit" name="submit">Submit Ticket</button>
</form>
<script type="text/javascript">
	if ( window.history.replaceState ) {
	  window.history.replaceState( null, null, window.location.href );
	}
</script>
	<?php
}

function citadel_save_ticket_if_submitted() {
	// Stop running function if form wasn't submitted
	if ( !isset($_POST['title']) ) {
		return;
	}

	if(!empty($_POST['website'])) {
		return;
	}

	// Check that the nonce was set and valid
	if( !wp_verify_nonce($_POST['_wpnonce'], 'submit_ticket') ) {
		echo '<p style="text-align: center; color: #7D2935;"><strong>Did not save because your form seemed to be invalid. Sorry!</strong></p>';
		return;
	}

	// Do some minor form validation to make sure there is content
	if (strlen($_POST['title']) < 5) {
		echo 'Please enter a subject. Subjects must be at least five characters long.';
		return;
	}

	// Add the content of the form to $post as an array
	$post = array(
		'post_title'    => $_POST['title'],
		'post_content'  => $_POST['content'],
		'meta_input'	=> array(
							'citadel_submitter_key' => $_POST['submitter'] . '@citadel.edu',
							'citadel_submitter_name_key' => $_POST['submitter_name'],
						),
		'post_status'   => 'publish',
		'post_type' 	=> 'citadel_ticket',
	);
	$post_id = wp_insert_post( $post );
	wp_set_object_terms( $post_id, $_POST['status'], 'ticket_categories' );
	wp_set_object_terms( $post_id, $_POST['type'], 'ticket_types' );
	update_post_meta($post_id, 'citadel_submitter_key', $_POST['submitter'] . '@citadel.edu');
	update_post_meta($post_id, 'citadel_submitter_name_key', $_POST['submitter_name']);
	$ticket_link = get_post_type_archive_link( 'citadel_ticket' ) . '#' . $post_id;
	echo '<p style="text-align: center; color: green;"><strong><a href="' . $ticket_link . '">Ticket [#' . $post_id . ']</a> has been created!</strong></p>';
	//wp_redirect( $ticket_link );
}


add_filter( 'manage_citadel_ticket_posts_columns', 'citadel_filter_posts_columns' );
function citadel_filter_posts_columns( $columns ) {

	$columns = array(
		'cb' 		=> $columns['cb'],
		'id' 		=> __( 'ID' ),
		'title' 	=> __( 'Title' ),
		'status' 	=> __( 'Status' ),
		'type' 		=> __( 'Type' ),
		'submitter' => __( 'Submitter Email' ),
		'submitter_name' => __( 'Submitter Name' ),
		'date' 		=> __( 'Date' ),
	);
	return $columns;

}

add_action( 'manage_citadel_ticket_posts_custom_column', 'citadel_ticket_column', 10, 2);
function citadel_ticket_column( $column, $post_id ) {
	
	if ( 'id' === $column ) {
		echo $post_id;
	}

	if ( 'status' === $column ) {
		$status = get_the_terms( $post_id, 'ticket_categories' );

		if ( ! $status ) {
			_e( 'n/a' );  
		} else {
			echo $status[0]->name;
		}
	}

	if ( 'type' === $column ) {
		$type = get_the_terms( $post_id, 'ticket_types' );

		if ( ! $type ) {
			_e( 'n/a' );  
		} else {
			echo $type[0]->name;
		}
	}

	if ( 'submitter' === $column ) {
		$submitter = get_post_meta( $post_id, 'citadel_submitter_key', true );

		if ( ! $submitter ) {
			_e( 'n/a' );  
		} else {
			echo '<a href="mailto:' . $submitter . '?subject=Webmaster Ticket [#' . $post_id . ']: ' . get_the_title($post_id) . '&body=%0D%0A%0D%0A-----%0D%0A%0D%0A[#' . $post_id . ']: ' . get_the_title($post_id) . '%0D%0A%0D%0A' . wp_strip_all_tags( get_the_content($post_id) ) . '%0D%0A%0D%0A-----">' . $submitter . '</a>';
		}
	}

	if ( 'submitter_name' === $column ) {
		$submitter_name = get_post_meta( $post_id, 'citadel_submitter_name_key', true );

		if ( $submitter_name ) {
			echo $submitter_name;
		}
	}
}

add_filter( 'manage_edit-citadel_ticket_sortable_columns', 'citadel_ticket_sortable_columns');
function citadel_ticket_sortable_columns( $columns ) {
	$columns['id'] = 'ID';
	return $columns;
}

add_action( 'admin_menu', 'add_ticket_menu_bubble' );
function add_ticket_menu_bubble() {
	global $menu;

	$args = array(
		'taxonomy' => 'ticket_categories',
        'hide_empty' => true,
        'exclude'    => array(7, 8),
	);

	$cats = get_categories($args);

	$open_count = array();

	foreach ($cats as $cat) {
		$open_count[] = $cat->count;
	}

	$open_count = array_sum($open_count);

	if ( $open_count ) {

		foreach ( $menu as $key => $value ) {

			if ( $menu[$key][2] == 'edit.php?post_type=citadel_ticket' ) {

				$menu[$key][0] .= ' (' . $open_count . ')';

				return;
			}
		}
	}
}

// Table pagination
function my_pagination_rewrite() {
    add_rewrite_rule('tickets/page/?([0-9]{1,})/?$', 'index.php?category_name=tickets&paged=$matches[1]', 'top');
    add_rewrite_rule( '^tickets/status/([^/]+)/page/([0-9]+)?$', 'index.php?post_type=citadel_ticket&ticket_categories=$matches[1]&paged=$matches[2]', 'top' );
    add_rewrite_rule( '^tickets/type/([^/]+)/page/([0-9]+)?$', 'index.php?post_type=citadel_ticket&ticket_types=$matches[1]&paged=$matches[2]', 'top' );
}
add_action('init', 'my_pagination_rewrite');
