<?php get_header(); ?>
 
	<article id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
 
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();
		?>
		<?php
			$category = get_the_category(get_the_ID());
			$cat_name = $category[0]->name;
			$cat_link = get_category_link($category[0]->cat_ID);
			$parent_name = get_cat_name($category[0]->parent);
			$parent_link = get_category_link($category[0]->parent);
		?>
		<p class="breadcrumb"><a href="<?php echo site_url(); ?>">Solutions</a><i class="fas fa-angle-right"></i><a href="<?php echo $parent_link; ?>"><?php echo $parent_name; ?></a><i class="fas fa-angle-right"></i><a href="<?php echo $cat_link; ?>"><?php echo $cat_name; ?></a></p>
		<h1 class="entry-title"><?php echo the_title(); ?></h1>

		<p class="modified-time">last modified at <?php echo the_modified_date('g:i a'); ?> on <?php echo the_modified_date('F j, Y'); ?></p>

		<div class="entry-content"><?php echo the_content(); ?></div>

		<?php

		$related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 5, 'post__not_in' => array($post->ID) ) );
		if( $related ):
		?>
		<section class="related">
			<h3>Related Topics in <a href="<?php echo $parent_link; ?>"><?php echo $parent_name; ?></a><i class="fas fa-angle-right"></i><a href="<?php echo $cat_link; ?>"><?php echo $cat_name; ?></a></h3>
		<?php
			foreach( $related as $post ) {
			setup_postdata($post);
		?>
		 <ul> 
			<li>
				<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a>
			</li>
		</ul>

		<?php } ?>

		</section>

		<?php
			endif;
			wp_reset_postdata();

			// End the loop.
			endwhile;
		?>
 
		</main><!-- .site-main -->
	</article><!-- .content-area -->
 
<?php get_footer(); ?>