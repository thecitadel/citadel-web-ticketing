jQuery(document).ready(function($) {

	// Pushes footer to bottom of page if content is short
	function footerPosition() {
		var headerHeight = $('#main-header').outerHeight(),
			pageHeight = $('#page').outerHeight(),
			footerHeight = $('#footer').outerHeight(),
			windowHeight = $(window).height(),
			totalHeight = headerHeight + pageHeight + footerHeight;

		if (windowHeight > totalHeight) {
			$('#footer').css('position', 'absolute');
		} else {
			$('#footer').css('position', '');
		}
	}

	$('#submitTicket input[name="submitter"]').keypress(function (e) {
		var regex = new RegExp("^[a-zA-Z0-9]+$");
		var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
		if (regex.test(str)) {
			return true;
		}

		e.preventDefault();
		return false;
	});

	if ($('body').hasClass('post-type-archive-citadel_ticket')) {
		if(window.location.hash) {
			var hash = window.location.hash.substring(1),
				hash = '#' + hash;

			$(hash).addClass('active');
		}
	}

	$('#ticketTable td select').each(function() {
		var column = $(this).parent().prop('className'),
			original = $('option.original', this),
			original_value = original.val();

		$(this).change(function() {
			
			var button = $('#ticketTable .edit-tickets').find('.' + column).find('button')

			if (!$('option:selected', this).hasClass('original')) {
				original.removeAttr('selected');
				$(this).parent().addClass('edited');
				button.show();
			} else {
				$(this).parent().removeClass('edited');
				if (!$('#ticketTable').find('.' + column + '.edited').length) {
					button.hide();
				}
			}	
			
		});
	});

	$('#ticketTable .edit-tickets button').each(function() {
		var column = $(this).parent().prop('className');

		$(this).click(function() {
			$('#ticketTable .' + column + '.edited').each(function() {
				var post_id = $(this).parent().attr('id'),
					changed_value = $(this).find('option:selected').val();

				$.ajax({
					type: 'POST',
					url: template_directory.templateUrl + '/template-parts/tickets/submit-ticket-changes.php',
					data: {
						id: post_id,
						column: column,
						value: changed_value,
					},
					success: function(data) {
						console.log(data);
						$(this).removeClass('edited');
					},
					error: function() {
						console.log('error!');
					},
				});	
			});
		})
	});
	
	// Functions to call on page load
	footerPosition();

	//Functions to call when window is resized
	jQuery(window).resize(function($) {
		footerPosition();
	});

	//Functions to call when window is scrolled
	jQuery(window).scroll(function($) {
		
	});
	
});