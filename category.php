<?php get_header(); ?>

<article id="content" class="archive">
	<section class="category-group">
	<?php
		$term = get_queried_object();
		$children = get_terms( $term->taxonomy, array(
			'parent'		=> $term->term_id,
			'hide_empty' 	=> false,
		) );

		if (!$children) :
			$parent_name = get_cat_name($term->parent);
			$parent_link = get_category_link($term->parent);
	?>
		<p class="breadcrumb"><a href="<?php echo site_url(); ?>">Solutions</a><i class="fas fa-angle-right"></i><a href="<?php echo $parent_link; ?>"><?php echo $parent_name; ?></a></p>
	<?php else: ?>
		<p class="breadcrumb"><a href="<?php echo site_url(); ?>">Solutions</a></p>
	<?php endif; ?>
		<h1><?php single_cat_title(); ?> <span>(<?php echo wp_get_cat_postcount($term->term_id); ?>)</span></h1>

	<?php if ($children) : ?>
		
		<?php
			$subcat_args = array(
				'post_status'	=> 'publish',
				'hide_empty' 	=> 0,
				'parent' => $term->term_id,
			);

			$subcategories = get_categories($subcat_args);

			if ($subcategories != 0):
		?>

		<ul class="sub-category">

		<?php
			foreach ($subcategories as $subcategory) {
				$subcategory_link = get_category_link( $subcategory->cat_ID );
		?>

		<li><a href="<?php echo esc_url( $subcategory_link ); ?>"><?php echo $subcategory->name; ?> <span>(<?php echo $subcategory->category_count; ?>)</span></a>

		<?php
			$post_args = array(
				'posts_per_page' 	=> -1,
				'orderby'	 		=> 'name',
				'order' 	 		=> 'ASC',
				'cat' 				=> $subcategory->cat_ID,
			);

			$posts = query_posts($post_args);

			if(have_posts()) :
		?>

		<ul class="posts">

		<?php while (have_posts()) : the_post();
		?>

		<li><a href="<?php the_permalink(); ?>" class="post-thumbnail" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>

		<?php endwhile; ?>

		</ul>

		<?php endif; wp_reset_postdata(); ?>

		</li>

		<?php } ?>

		</ul>

		<?php endif; ?>

	<?php else: ?>
		<?php
			$post_args = array(
				'posts_per_page' 	=> -1,
				'orderby'	 		=> 'name',
				'order' 	 		=> 'ASC',
				'cat' 				=> $subcategory->cat_ID,
			);

			$posts = query_posts($post_args);

			if(have_posts()) :
		?>

		<ul class="posts">

		<?php while (have_posts()) : the_post();
		?>

		<li><a href="<?php the_permalink(); ?>" class="post-thumbnail" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>

		<?php endwhile; ?>

		</ul>
		<?php endif; ?>
	<?php endif; ?>
	</section>
</article>

<?php get_footer(); ?>