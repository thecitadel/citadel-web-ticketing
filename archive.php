<?php get_header(); ?>
<article id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
	
		<header class="entry-header">
			<h1 class="entry-title"><?php the_title(); ?></h1>
		</header><!-- .entry-header -->
		<div class="entry-content">
			<?php the_content(); ?>
		</div>
	
	</main>
</article>
<?php get_footer(); ?>