<?php get_header(); ?>
<article id="primary" class="content-area<?php if ( current_user_can( 'manage_options' ) ) : ?> admin-view<?php endif; ?>">
	<main id="main" class="site-main" role="main">

	<header class="entry-header">
		<h1 class="entry-title">Citadel Webmaster Tickets</h1>
	</header><!-- .entry-header -->
	<p class="submit-ticket-link"><a href="<?php echo site_url(); ?>/submit-ticket">Submit a Ticket</a></p>
	<div class="entry-content">
		<?php echo the_content(); ?>
		<?php get_template_part( 'template-parts/tickets/content', 'ticket_table_sorting' ); ?>
		<?php get_template_part( 'template-parts/tickets/content', 'ticket_table' ); ?>
	</div>

	</main>
</article>
<?php get_footer(); ?>