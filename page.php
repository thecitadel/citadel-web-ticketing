<?php get_header(); ?>

<?php while (have_posts()) : the_post(); ?>

	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->
	<div class="entry-content">
		<?php echo the_content(); ?>
	</div>

<?php endwhile; ?>

<?php get_footer(); ?>