<?php

global $query_string;

wp_parse_str( $query_string, $search_query );
$search = new WP_Query( $search_query );

get_header();

?>
<article id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
	<?php if ( have_posts() ) : ?>
		<h1 class="entry-title"><?php printf( __( 'Search results for "%s"', 'shape' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
		<section class="post-results">
			<?php while ( have_posts() ) : the_post(); ?>
				<div class="search-item">
					<h2><a href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a></h2>
					<?php if(has_excerpt()) : ?>
						<?php echo the_excerpt(); ?>
					<?php endif; ?>
				</div>
			<?php endwhile; ?>
		</section>
	<?php else : ?>

        <h1 class="entry-title">No results to display</h1>
        <p style="text-align: center;">Please use the searchbar above to try a different search.</p>

    <?php endif; ?>
	</main>
</article>
<?php get_footer(); ?>