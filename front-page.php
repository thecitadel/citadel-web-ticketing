<?php get_header(); ?>

<article id="content">
	<h1 class="entry-title"><?php echo bloginfo('name'); ?></h1>
	<?php
		$cat_args = array(
			'post_status'	=> 'publish',
			'hide_empty' 	=> true,
			'parent' => 0,
		);

		$categories = get_categories($cat_args);

		foreach ($categories as $category) {
			$category_link = get_category_link( $category->cat_ID );

	?>
	<section class="category-group">
		<div class="main-category">
			<h2><a href="<?php echo esc_url( $category_link ); ?>"><?php echo $category->name; ?> <span>(<?php echo wp_get_cat_postcount($category->cat_ID); ?>)</span></a></h2>
			<?php
				$subcat_args = array(
					'post_status'	=> 'publish',
					'hide_empty' 	=> true,
					'parent' => $category->cat_ID,
				);

				$subcategories = get_categories($subcat_args);

				if ($subcategories != 0):
			?>

			<ul class="sub-category">

			<?php
				foreach ($subcategories as $subcategory) {
					$subcategory_link = get_category_link( $subcategory->cat_ID );
			?>

			<li><a href="<?php echo esc_url( $subcategory_link ); ?>"><?php echo $subcategory->name; ?> <span>(<?php echo $subcategory->category_count; ?>)</span></a>

			<?php
				$post_args = array(
					'posts_per_page' 	=> -1,
					'orderby'	 		=> 'name',
					'order' 	 		=> 'ASC',
					'cat' 				=> $subcategory->cat_ID,
				);

				$posts = query_posts($post_args);

				if(have_posts()) :
			?>

			<ul class="posts">

			<?php while (have_posts()) : the_post();
			?>

			<li><a href="<?php the_permalink(); ?>" class="post-thumbnail" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>

			<?php endwhile; ?>

			</ul>

			<?php endif; wp_reset_postdata(); ?>

			</li>

			<?php } ?>

			</ul>

			<?php endif; ?>
		</div>
	</section>
	<?php } ?>

</article>

<?php get_footer(); ?>