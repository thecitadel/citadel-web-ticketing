<?php

	$term = get_queried_object();
	$current_cat = $term->slug;
	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

	if (is_tax('ticket_categories')) :
		$args = array( 
			'orderby' 			=> 'date',
			'order' 			=> 'DESC',
			'post_type' 		=> 'citadel_ticket',
			'post_status'		=> 'publish',
			'tax_query'			=> array(
										array(
											'taxonomy' 	=> 'ticket_categories',
											'field'		=> 'slug',
											'terms' 	=> $current_cat,
										),
									),
			'posts_per_page' 	=> 25,
			'paged' 			=> $paged,
		);
	elseif (is_tax('ticket_types')) :
		$args = array( 
			'orderby' 			=> 'date',
			'order' 			=> 'DESC',
			'post_type' 		=> 'citadel_ticket',
			'post_status'		=> 'publish',
			'tax_query'			=> array(
										array(
											'taxonomy' 	=> 'ticket_types',
											'field'		=> 'slug',
											'terms' 	=> $current_cat,
										),
									),
			'posts_per_page' 	=> 25,
			'paged' 			=> $paged,
		);
	else :
		$args = array( 
			'orderby' 			=> 'date',
			'order' 			=> 'DESC',
			'post_type' 		=> 'citadel_ticket',
			'post_status'		=> 'publish',
			'posts_per_page' 	=> 25,
			'paged' 			=> $paged,
		);
	endif;

	$the_query = new WP_Query( $args );
?>
<?php if ( $the_query->have_posts() ) : ?>
<div class="table-container">
	<table id="ticketTable">
		<thead>
			<tr>
				<th class="number">ID</th>
				<th class="date">Created</th>
				<?php if ( current_user_can( 'manage_options' ) ) : ?><th class="subject">Subject</th><?php endif; ?>
				<th class="type">Type</th>
				<th class="status">Status</th>
				<?php if ( current_user_can( 'manage_options' ) ) : ?><th class="submitter">Submitter</th><?php endif; ?>
			</tr>
		</thead>
		<tbody>

		<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
			<?php
				$post_id = get_the_ID();
				$categories = get_the_terms( get_the_ID(), 'ticket_categories' );
				$cat_name = $categories[0]->name;
				$cat_slug = $categories[0]->slug;
				$user = wp_get_current_user();
				$types = get_the_terms( get_the_ID(), 'ticket_types' );
			?>
			<tr class="<?php echo $cat_slug; ?>" id="<?php echo get_the_ID(); ?>">
				<td class="number">#<?php echo get_the_ID(); ?></td>
				<td class="date"><span class="nowrap"><?php echo get_the_date('m/d/Y', $post_id); ?></span> at <span class="nowrap"><?php echo get_the_date('g:i a', $post_id); ?></span></td>
			<?php if ( current_user_can( 'manage_options' ) ) : ?>
				<td class="subject">
					<?php if ( current_user_can( 'manage_options' ) ) : ?>
						<a href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a>
					<?php else: ?>
						<?php echo the_title(); ?>
					<?php endif; ?>
				</td>
			<?php endif; ?>
				<td class="<?php echo $types[0]->slug; ?> type"><?php echo $types[0]->name; ?></td>
				<td class="status">
					<?php if ( current_user_can( 'manage_options' ) ) : ?>
						<select>
						<?php
							$args = array(
									   'taxonomy' 	=> 'ticket_categories',
									   'orderby' 	=> 'name',
									   'order'   	=> 'ASC',
									   'hide_empty' => false,
								   );

							$cats = get_categories($args);

							foreach($cats as $cat) {
								$category_slug = $cat->slug;
						?>
							<?php if ($category_slug == $cat_slug) : ?>
								<option value="<?php echo $cat_slug; ?>" selected class="original"><?php echo $cat->name; ?> *</option>
							<?php else : ?>
								<option value="<?php echo $category_slug; ?>"><?php echo $cat->name; ?></option>
							<?php endif; ?>

						<?php } ?>
						</select>

					<?php else : ?>

						<?php echo $cat_name; ?>

					<?php endif; ?>

				</td>
			<?php if ( current_user_can( 'manage_options' ) ) : ?>
				<td class="submitter">
					<a href="mailto:<?php echo get_post_meta( get_the_ID(), 'citadel_submitter_key', true ); ?>?subject=Webmaster Ticket [#<?php echo the_ID(); ?>]: <?php echo the_title(); ?>&body=%0D%0A%0D%0A-----%0D%0A%0D%0A[#<?php echo the_ID(); ?>]: <?php echo the_title(); ?>%0D%0A%0D%0A<?php echo wp_strip_all_tags( get_the_content() ); ?>%0D%0A%0D%0A-----" title="<?php echo get_post_meta( get_the_ID(), 'citadel_submitter_name_key', true ); ?>"><?php echo get_post_meta( get_the_ID(), 'citadel_submitter_key', true ); ?></a>
				</td>
			<?php endif; ?>
			</tr>
		<?php endwhile; ?>
			<?php if ( current_user_can( 'manage_options' ) ) : ?>
			<tr class="edit-tickets">
				<td class="number"></td>
				<td class="date"></td>
				<td class="subject"></td>
				<td class="type"></td>
				<td class="status"><button>Save</button></td>
				<td class="submitter"></td>
			</tr>
			<?php endif; ?>

		</tbody>
	</table>

	<div class="pagination">
		<?php 
			echo paginate_links( array(
				'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
				'total'        => $the_query->max_num_pages,
				'current'      => max( 1, get_query_var( 'paged' ) ),
				'format'       => '?paged=%#%',
				'show_all'     => false,
				'type'         => 'plain',
				'end_size'     => 2,
				'mid_size'     => 1,
				'prev_next'    => true,
				'add_args'     => false,
				'add_fragment' => '',
			) );
		?>
	</div>

</div>
<?php else: ?>

<p style="text-align: center;">Sorry, there are no tickets to display.</p>

<?php endif; ?>
<?php wp_reset_query(); ?>