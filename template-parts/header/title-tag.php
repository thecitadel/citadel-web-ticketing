<?php 

if ( is_front_page() || is_home() ) {
	echo get_bloginfo( 'name' );
} else if ( is_404() ) { ?>
	404 | <?php echo get_bloginfo( 'name' ); 
} else if ( is_category() ) {
	$term = get_queried_object();
	$cat_name = get_cat_name($term->term_id);
?>
	<?php echo $cat_name; ?> | <?php echo get_bloginfo( 'name' );
} else if ( is_search() ) { ?>
	Search results for "<?php echo get_search_query(); ?>" | <?php echo get_bloginfo( 'name' );
} else if ( ('citadel_ticket' == get_post_type()) && (is_single()) ) { ?>
	[#<?php echo the_ID(); ?>] <?php echo the_title(); ?> | <?php echo get_bloginfo( 'name' ); 
} else if ( (is_tax('ticket_categories')) ) {
	$term = get_queried_object();

	echo $term->name; ?> Tickets | <?php echo get_bloginfo( 'name' ); 

} else if ( (is_tax('ticket_types')) ) {
	$term = get_queried_object();
?>
	Tickets with type "<?php echo $term->name; ?>" | <?php echo get_bloginfo( 'name' );

} else if ( is_archive('citadel_ticket') ) {
?>
	Tickets | <?php echo get_bloginfo( 'name' );

} else {
	the_title(); ?> | <?php echo get_bloginfo( 'name' );
}

?>

