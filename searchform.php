<?php $unique_id = esc_attr( uniqid( 'search-form-' ) ); ?>

<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label for="s">Search <?php echo bloginfo('name'); ?></label>
	<input type="text" id="s" class="search-field" placeholder="Search <?php echo bloginfo('name'); ?>..." value="<?php echo get_search_query(); ?>" name="s" />
	<button type="submit" class="search-submit" aria-label="Search The Citadel Web Helpdesk"><span class="screen-reader-text"><i class="fas fa-search"></i></span></button>
</form>