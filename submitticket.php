<?php /* Template Name: Submit Ticket */ ?>

<?php get_header(); ?>
<article id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
	<?php while (have_posts()) : the_post(); ?>
	<p class="all-tickets"><a href="<?php echo get_post_type_archive_link( 'citadel_ticket' ); ?>"><i class="fas fa-angle-left"></i> All Tickets</a></p>
	<header class="entry-header ticketing">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->
	<div class="entry-content">
		<?php echo the_content(); ?>
		<?php echo do_shortcode("[citadel_ticket_submit]"); ?>
	</div>
	<?php endwhile; ?>
	</main>
</article>
<?php get_footer(); ?>