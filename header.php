<!doctype html>
<html <?php language_attributes(); ?>>
<head itemtype="http://schema.org/WebApplication" itemscope>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5">
<title><?php include 'template-parts/header/title-tag.php' ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
<?php include 'headcode.php' ?>
<?php wp_head(); ?>
</head>
<body <?php if (('citadel_ticket' == get_post_type())) : body_class('ticketing'); else : body_class(); endif; ?>>

	<!-- Skip navigation for screen readers -->
	<a href="#primary-nav" class="skip">Skip to main navigation</a>
	<a href="#main-content" class="skip">Skip to main content</a>

	<!-- Start Header -->
	<header id="main-header">
		<div id="citadel-heading" class="container">
			<div class="header-img">
				<a href="<?php echo site_url(); ?>">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/citadel-logo-white.png" alt="<?php echo get_bloginfo( 'name' ); ?>">
				</a>
			</div>
			<div class="right-header">
				<?php if ( has_nav_menu( 'primary' ) ): ?>
				<nav id="primary-nav">
					<?php wp_nav_menu( array(
						'theme_location' => 'primary',
						'menu_id'        => 'primary',
						'container' 	 => '',
					) ); ?>
				</nav>
				<?php endif; ?>
			</div>
		</div>
	</header>
	<!-- End Header -->

	<!-- Start Page Content -->
	<main id="page" class="container">
		<?php if (!('citadel_ticket' == get_post_type())) : ?>
			<?php include 'searchform.php'; ?>
		<?php endif; ?>