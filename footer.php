</main>
<!-- End Page Content -->

<!-- Start Footer -->
<footer id="footer">

	<section class="main-footer">
		<div class="container">
			<span>&copy; <?php echo date('Y'); ?> <a href="http://citadel.edu">The Citadel</a>. All rights reserved.</span>
		</div>
	</section>

</footer>
<!-- End Footer -->

<?php wp_footer(); ?>
<?php include 'footercode.php' ?>
</body>
</html>