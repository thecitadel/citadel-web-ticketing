<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

<a href="<?php network_site_url() ?>post-new.php?post_type=page" class="quick-link">
	<i class="fas fa-fw fa-file"></i>
	<p>Add New Page</p>
</a>
<a href="<?php network_site_url() ?>my-sites.php" class="quick-link">
	<i class="fas fa-fw fa-th-list"></i>
	<p>My Sites</p>
</a>
<a href="https://citadelwebmaster.freshdesk.com/support/tickets" target="_blank" class="quick-link">
	<i class="fas fa-fw fa-ticket-alt"></i>
	<p>Webmaster Request</p>
</a>
<a href="https://citadelwebmaster.freshdesk.com/support/home" target="_blank" class="quick-link">
	<i class="fas fa-fw fa-book"></i>
	<p>WordPress Knowledge Base</p>
</a>
<a href="https://new.monsido.com/3457/" target="_blank" class="quick-link">
	<i class="fas fa-fw fa-universal-access"></i>
	<p>Accessibility &amp; SEO Tool</p>
</a>
<a href="#" target="_blank" class="quick-link">
	<i class="fas fa-fw fa-file-invoice"></i>
	<p>Citadel Web Policy</p>
</a>